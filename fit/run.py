import os
import platform
import subprocess as sub
import time

if platform.system() == 'Windows':
    import winsound

import fitter as ft

def signalise_run_finish(i):
    if platform.system() == 'Windows':
        duration = 1000  # milliseconds
        freq = 440  # Hz
        winsound.Beep(freq, duration)
        print(f'The run {i} on Windows is done')
    else:
        pass


def run():
    os_ = platform.system()
    if os_ == 'Windows':
        # ObsData_instance = ft.ObsData(0)
        # FittingData_instance = ft.FittingData(ObsData_instance)
        for i in range(1,2):
            start_time = time.time()
            SyntheticData_instance = ft.SyntheticData(key=0)
            FittingData_instance = ft.FittingData(SyntheticData_instance, seed=i)
            FittingData_instance.fit(SyntheticData_instance)
            print('Time taken is {}'.format(time.time() - start_time))
            # FittingData_instance.profile_calculate(SyntheticData_instance, 2.8)
            # FittingData_instance._calculate_diff_central_core(SyntheticData_instance, 2.8)
            signalise_run_finish(i)

    elif os_ == 'Linux':
        n_to_generate = 5
        # TODO: check the runner on the cluster, it is another one
        for i in range(n_to_generate):
            task = f'ipython fitter.py'
            with open('sub_fitter.sh', 'w+') as f:
                f.write(f'#!/bin/bash \n\n#SBATCH -p RT \n#SBATCH -J fitter{i} \n#SBATCH -N 1 \n#SBATCH -n 1 \n')
                f.write('#SBATCH -o reports/%j.out\n\n')
                f.write('sleep 100')
            # time.sleep(1)
            # '''
            # The sleeping intends to increase the level of randomness as
            # python has the following multiprocessing and multithreading problem:
            # it initializes subprocesses with the same random state inherited from the parent.
            # The problem holds (to some extent) even in the presented form of creating a number of processes:
            # some of them are initialized with the same random state and passing numpy seed is somehow ignored.
            # This problem is recognized in the community.
            # '''
            _ = sub.run('sbatch sub_fitter.sh', capture_output=True, text=True, shell=True, check=True)


# run()

SyntheticData_instance = ft.SyntheticData(key=None)
SyntheticData_instance = ft.SyntheticData(key=None)
SyntheticData_instance = ft.SyntheticData(key=2)
SyntheticData_instance = ft.SyntheticData(key=3)
