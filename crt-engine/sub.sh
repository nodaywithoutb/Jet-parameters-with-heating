#!/bin/bash 

#SBATCH -p RT 
#SBATCH -J my_test_tasks 
#SBATCH -N 1 
#SBATCH -n 1 
#SBATCH -o D:\main\astrophys\heating-fitting\storage\calculating_preglue\reports\%j.out

bin\intensity D:\main\astrophys\heating-fitting\data\crt_Data\mBs5\model1_s5_cross_M2in_5.040000_25RL.dat D:\main\astrophys\heating-fitting\storage\calculating_preglue\out_s5d25_phi15_regular_1_49970.dat 15 15.4 10.556331067110412 0.01 1 1 6.8 2 1 1 1e-05 1e-05 1 20 49970 49973