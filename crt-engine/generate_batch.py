import os
import os.path
from os.path import join
import matplotlib.pyplot as plt
import numpy as np
import subprocess as sub
import shutil

def project_path():
    return os.path.abspath(os.path.join(os.getcwd(), os.pardir))

import sys
sys.path.append(project_path())
sys.path.append(join(project_path(), 'data'))
sys.path.append(join(project_path(), 'genmodelling'))

import data.mhddata as md
import genmodelling as gm

preglue_path = join(join(project_path(), 'storage'), 'calculating_preglue')
glued_path = join(join(project_path(), 'storage'), 'calculating_glued')
trash_path = join(join(project_path(), 'storage'), 'calculating_trash')
crt_path = join(project_path(), 'crt-engine')
# This is the default value for initial, precision reference calculation.
prec_abs = 10 ** (-5)
# Initial step size is equal to a step from MHD files. Later it is dynamically adjusted by odeint integration.
in_step_mult = 1

progs = {}

def psi0_tau10(tau, tau10):
    return (tau / tau10) ** 0.25 * 10


def generate_string(sf, of, phi, nu, psi0, rl, lower, lower_coord, sigma_m_c, p, gmin, ordered, prec_rel,
                    reduce_multiplier, starting_coord, ending_coord):
    str_calc = (sf
                + ' ' + of
                + ' ' + str(phi)
                + ' ' + str(nu)
                + ' ' + str(psi0)
                + ' ' + str(rl)
                + ' ' + str(lower) + ' ' + str(lower_coord)
                + ' ' + str(sigma_m_c)
                + ' ' + str(p) + ' ' + str(gmin) + ' ' + str(ordered)
                + ' ' + str(prec_abs) + ' ' + str(prec_rel)
                + ' ' + str(in_step_mult) + ' ' + str(reduce_multiplier)
                + ' ' + str(starting_coord) + ' ' + str(ending_coord))
    return str_calc


def generate_tasks(m, sigma_m, d_rl, phi, nu, p, gmin, ordered, taus, mode, type, prec_rel, reduce_multiplier,
                   max_tasks_allowed, starting_coord_fix, ending_coord_fix):
    tasks = []

    if type == 'regular':
        lower = 1
        lower_coord = 1
    elif type == 'l0.01l0.95':
        lower = 0.01
        lower_coord = 0.95
    elif type == 'l0.001l0.95':
        lower = 0.001
        lower_coord = 0.95
    elif type == 'Ohmic':
        lower = 3
        lower_coord = 1

    sp, sf = md.get_source(m, sigma_m, d_rl)
    sp = join(project_path(), sp)
    sf = join(sp, sf)

    if mode == 'BHspin':
        rl_pc = {'25': '0.01',
                 '44': '0.0057',
                 '75': '0.0033',
                 '100': '0.0025',
                 '140': '0.0018'}
    elif mode == 'd25iterated':
        rl_pc = {'25': '0.01',
                 '100': '0.01'}

    if m == 'L':
        sigma_m_c = float(sigma_m)
    elif m == 'B':
        sigma_m_c = md.sigma_m_B[str(sigma_m)]

    with open(join(sp, sf)) as f:
        lim = len(f.readlines()) * 2 + 1

    assert max_tasks_allowed < lim
    if len(starting_coord_fix) > 0:
        ranges = [starting_coord_fix, ending_coord_fix]
    else:
        ranges = np.arange(0, lim + lim // max_tasks_allowed, lim // max_tasks_allowed)

    tau10 = md.tau10_max[type][m]['s' + str(sigma_m)]['d' + str(d_rl)][str(phi)]

    for j in range(len(taus)):
        for i in range(len(ranges) - 1):
            starting_coord = ranges[i]
            ending_coord = ranges[i + 1]
            assert starting_coord < lim
            if ranges[i + 1] > lim:
                ending_coord = lim

            output_file = ('out_' + m + 's' + str(sigma_m) + 'd' + str(d_rl) + '_phi' + str(phi) + '_' + type + '_'
                           + str(taus[j]) + '_' + str(starting_coord) + '.dat')
            of = join(preglue_path, output_file)
            psi0 = psi0_tau10(taus[j], tau10)
            rl = rl_pc[str(d_rl)]

            str_calc = generate_string(sf, of, phi, nu, psi0, rl, lower, lower_coord, sigma_m_c, p, gmin, ordered,
                                       prec_rel,
                                       reduce_multiplier, starting_coord, ending_coord)
            tasks.append(str_calc)

    assert len(tasks) <= max_tasks_allowed + 2
    return tasks


def get_sc_ec(of, sf, max_tasks_allowed):
    sc = ''
    char_index = of.find('.dat') - 1  # So the pointer is at the last digit of starting_coord.
    char = of[char_index]
    while char != '_':
        sc += char
        char_index -= 1
        char = of[char_index]
    sc = sc[::-1]

    with open(sf) as f:
        lim = len(f.readlines()) * 2 + 1

    ec = sc + lim // max_tasks_allowed

    if ec > lim:
        ec = lim
    return sc, ec


def run_sbatch(binary, tasks, partition, tasks_title):
    reports = join(preglue_path, 'reports')
    if os.path.exists(reports):
        pass
    else:
        os.mkdir(reports)
    for task in tasks:
        task = join('bin', binary) + ' ' + task
        with open('sub.sh', 'w+') as f:
            f.write('#!/bin/bash \n\n#SBATCH -p ' + partition
                    + ' \n#SBATCH -J ' + tasks_title
                    + ' \n#SBATCH -N 1 \n#SBATCH -n 1 \n')
            f.write('#SBATCH -o ' + join(reports, '%j.out') + '\n\n')
            f.write(task)
        _ = sub.run('sbatch sub.sh', capture_output=True, text=True, shell=True, check=True)


def calculate_tasks(binary, partition, tasks_title, m, sigma_m, d_rl, phi, nu, p, gmin, ordered, taus, mode, type,
                    reduce_multiplier, max_tasks_allowed):
    prec_rel = prec_abs
    tasks = generate_tasks(m, sigma_m, d_rl, phi, nu, p, gmin, ordered, taus, mode, type, prec_rel, reduce_multiplier,
                           max_tasks_allowed, '', '')

    sp, sf = md.get_source(m, sigma_m, d_rl)
    sp = join(project_path(), sp)
    sf = join(sp, sf)

    with open(sf) as f:
        lim = len(f.readlines()) * 2 + 1
    ranges = np.arange(0, lim + lim // max_tasks_allowed, lim // max_tasks_allowed)

    while len(tasks) > 0:
        run_sbatch(binary, tasks, partition, tasks_title)

        tasks = []
        prec_rel /= 10

        for of in os.listdir(preglue_path):
            R, I = np.loadtxt(of, unpack=True)
            for i in range(len(I) - 2):
                if I[i] < I[i + 1] < I[i + 2]:
                    pass
                elif I[i] > I[i + 1] > I[i + 2]:
                    pass
                else:
                    plt.plot(R, I)
                    plt.show()

                    print('Print yes to update, print no to pass')
                    decision = input()

                    if decision == 'yes':
                        sc, ec = get_sc_ec(of, sf, max_tasks_allowed)
                        tasks.append(generate_tasks(
                            m, sigma_m, d_rl, phi, nu, p, gmin, ordered, taus, mode, type, prec_rel,
                            reduce_multiplier, max_tasks_allowed, sc, ec))
                        shutil.move(join(preglue_path, of), join(trash_path, of))

    for j in range(len(taus)):
        gf = join(glued_path, 'out_' + m + 's' + str(sigma_m) + 'd' + str(d_rl) + '_phi' + str(phi)
                               + '_' + type + '_' + str(taus[j]) + '.dat')
        glued_file = open(gf, 'w+')
        for i in range(len(ranges)):
            if ranges[i] < lim:
                preglue_file = open(join(preglue_path, 'out_' + m + 's' + str(sigma_m) + 'd' + str(d_rl) + '_phi'
                                         + str(phi) + '_' + type + '_' + str(taus[j]) + '_' + str(ranges[i]) + '.dat'),
                                    'r')
                glued_file.write(preglue_file.read())
        glued_file.close()

        R, I = np.loadtxt(gf, unpack=True)
        plt.plot(-R, I, label=taus[j], color=gm.colors[j])

    plt.legend()
    plt.show()

def compile_calculators():
    #TODO: Depending on the function parameters, either call predefined binaries, or compile cpp code for linux,
    # or compile cpp code for windows. Organize the dictionary (?) of the calculate_tesks functions with either cluster
    # calculations, or single one-after-one calculations, or pooling (depending on the available cores). Like,
    # choose: cluster, windows pc, linux pc, linux working station (rellab3).
    if sys.platform.startswith('linux'):
        prog_path = join(crt_path, 'bin')
    elif sys.platform.startswith('win'):
        prog_path = join(crt_path, 'win')

    for prog in ['intensity', 'OD']:
        progs[prog] = join(prog_path, prog)
        success = os.system(progs[prog])
        if success:
            pass
        else:
            if os.path.isdir(prog_path):
                pass
            else:
                os.mkdir(prog_path)

            make_projech = open('make_projech.sh', 'w+')
            make_projech.write('Something that compiles')
            make_projech.close()



