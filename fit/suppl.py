import numpy as np
import os
from os.path import join

def project_path():
    proj_name = 'hf_gradboost'
    if proj_name in os.path.abspath(join(os.getcwd(), os.pardir)):
        return os.path.abspath(join(os.getcwd(), os.pardir))
    elif proj_name in os.getcwd():
        return os.getcwd()


def gitkeep_path(path):
    gk = open(join(path, '.gitkeep'), 'w')
    gk.close()


def get_path(parent, me):
    path = join(parent, me)
    if os.path.isdir(path):
        pass
    else:
        os.mkdir(path)
        gitkeep_path(path)
    return path


def storage_path():
    return get_path(parent=project_path(), me='storage')
