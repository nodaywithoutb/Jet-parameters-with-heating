import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import platform
import time
from os.path import join
import subprocess as sub
from scipy.signal import find_peaks, find_peaks_cwt, fftconvolve, ricker, cwt
from scipy.ndimage import gaussian_filter1d
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
import hashlib
from IPython.display import display

from suppl import project_path, get_path, storage_path


sigma_m_B = {
    '5': 6.8,
    '20': 36.97,
    '50': 96,
    '200': 396  # ?
}


def gauss(x, height, center, width):
    return height * np.exp(-(x - center) ** 2 / (2 * width ** 2))


def doublegauss(x, h1, c1, w1, h2, c2, w2):
    return gauss(x, h1, c1, w1) + gauss(x, h2, c2, w2)


def triplegauss(x, h1, c1, w1, h2, c2, w2, h3, c3, w3):
    return gauss(x, h1, c1, w1) + gauss(x, h2, c2, w2) + gauss(x, h3, c3, w3)


multigauss = {
    '1': gauss,
    '2': doublegauss,
    '3': triplegauss
}


class Data:
    # The number of allowed iterations for multiple gaussian fitting.
    MAXFEV = 10 ** 20
    # The number of allowed pixels for convolution (else RAM may drop the process and kill everything).
    CONV_PIXEL_LIMIT = 10000

    # The paths for both observational and simulated data.
    obs_data_path = get_path(storage_path(), 'obs_data')
    obs_data_avail = join(obs_data_path, 'obs_data_available.dat')
    source_info = join(obs_data_path, 'source_info.dat')
    mhd_data_path = get_path(storage_path(), 'mhd_data')
    mhd_data_avail = join(mhd_data_path, 'mhd_data_available.dat')
    obs_data_columns = ['source', 'freq', 'separation', 'bmaj', 'bmin', 'bpa', 'pixel_obs_mas', 'n_of_peaks', 'path']
    source_columns = ['source', 'pc_in_mas']
    mhd_data_columns = ['m', 's', 'd', 'sp', 'sf']
    out_save_path = get_path(storage_path(), 'outs')
    log_save_path = get_path(storage_path(), 'logs')
    obs_data_avail_ = pd.read_csv(obs_data_avail, header=None, sep=' ', usecols=range(len(obs_data_columns)))
    obs_data_avail_.columns = obs_data_columns
    mhd_data_avail_ = pd.read_csv(mhd_data_avail, header=None, sep=' ', usecols=range(len(mhd_data_columns)))
    mhd_data_avail_.columns = mhd_data_columns
    source_info_ = pd.read_csv(source_info, header=None, sep=' ', usecols=range(len(source_columns)))
    source_info_.columns = source_columns

    figs_save_path = get_path(storage_path(), 'figs')
    # TODO Question: should we separate this datas between subclasses? Review later

    source = None
    pc_in_mas = None
    pixel_obs_mas = None

    def __init__(self, x_trans, y_intensity, interp_peaks, freq):
        if x_trans is not None:
            self.x_trans = x_trans
            self.y_intensity = y_intensity
            self.freq = freq
            # Extract the properties of a given intensity profile.
            self.width_mas = max(self.x_trans)
            if isinstance(self, FittingData):
                w = len(self.y_intensity)
                self.detect_peaks_idx = find_peaks(self.y_intensity[(w // 4): (3 * w // 4)])[0] + w // 4
            else:
                self.detect_peaks_idx = find_peaks(self.y_intensity)[0]

            self.y_detect_peaks = self.y_intensity[self.detect_peaks_idx]
            self.x_detect_peaks = self.x_trans[self.detect_peaks_idx]

            if interp_peaks is not None:
                self.interp_peaks = interp_peaks
            else:
                self.interp_peaks = len(self.detect_peaks_idx)

    @staticmethod
    def os_():
        os_ = platform.system()
        try:
            assert os_ == 'Linux' or os_ == 'Windows'
        except AssertionError:
            print('The operational system can be only Linux or Windows.')
        return os_

    def show_gauss_components(self):
        if isinstance(self, ObsData) or isinstance(self, SyntheticData):
            fig, ax = plt.subplots(1, 1)
            self.plot_data(ax)
            if self.os_() == 'Windows':
                plt.show()
            data_type = 'obs' if isinstance(self, ObsData) else 'synth'
            fig.savefig(join(Data.figs_save_path, "{}_{}.jpg".format(data_type, self.key)))

    def gauss_components(self):
        # Assign Gaussian components bounds and guesses.
        # The parameters are: height, center coordinate, width.
        gauss_components_bounds = {}
        gauss_components_guesses = {}

        alpha = 0.2
        no_limits = False

        center = {}
        if hasattr(self, 'doubled_width') and self.doubled_width:
            center['1'] = self.width_mas
            center['0'] = self.width_mas * (1 - self.k_r / 2)
            center['2'] = self.width_mas * (1 + self.k_r / 2)
        elif isinstance(self, FittingData):
            center['1'] = self.width_mas / 2
            center['0'] = self.width_mas * (1 - self.k_r)
            center['2'] = self.width_mas * self.k_r
        else:
            center['1'] = self.width_mas / 4
            center['0'] = self.width_mas / 2
            center['2'] = self.width_mas / 4 * 3
            no_limits = True

        print('center', center)

        for peak in range(self.interp_peaks):
            gauss_components_bounds['peak' + str(peak)] = {}
            if no_limits:
                gauss_components_bounds['peak' + str(peak)]['lower'] = np.array([0, 0, 0])
                gauss_components_bounds['peak' + str(peak)]['upper'] = np.array(
                    [max(self.y_intensity),
                     self.width_mas,
                     self.width_mas / 4])
            else:
                gauss_components_bounds['peak' + str(peak)]['lower'] = np.array([0, center[str(peak)] * (1 - alpha), 0])
                gauss_components_bounds['peak' + str(peak)]['upper'] = np.array(
                    [max(self.y_intensity),
                     center[str(peak)] * (1 + alpha),
                     self.width_mas / 4])

            # We iterate over the interpreted peaks.
            # If the number of actually detected peaks if lesser, then we
            # construct the fiducial guess; else we use the detected peaks.

            if peak < len(self.detect_peaks_idx):
                gauss_components_guesses['peak' + str(peak)] = np.array(
                    [self.y_detect_peaks[peak],
                     self.x_detect_peaks[peak],
                     self.width_mas / 8])

            gauss_components_guesses['peak' + str(peak)] = np.array(
                [self.y_detect_peaks[0],
                 center[str(peak)],
                 self.width_mas / 8])

        # Calculate Gaussian components.
        self.gauss_components_popt = {}
        lower_bounds = []
        upper_bounds = []
        guesses = []
        lower_bounds = np.array(lower_bounds)
        upper_bounds = np.array(upper_bounds)
        guesses = np.array(guesses)
        for peak in range(self.interp_peaks):
            lower_bounds = np.concatenate((lower_bounds,
                                           gauss_components_bounds['peak' + str(peak)]['lower']))
            upper_bounds = np.concatenate((upper_bounds,
                                           gauss_components_bounds['peak' + str(peak)]['upper']))
            guesses = np.concatenate((guesses,
                                      gauss_components_guesses['peak' + str(peak)]))

        print(lower_bounds)
        print(upper_bounds)
        print(guesses)

        popt, pcov = curve_fit(
            f=multigauss[str(self.interp_peaks)], xdata=self.x_trans, ydata=self.y_intensity, maxfev=Data.MAXFEV,
            bounds=(lower_bounds, upper_bounds), p0=guesses)
        self.gauss_components_popt = popt
        self.order_popt(self.interp_peaks)
        self.p_sigma = np.sqrt(np.diag(pcov))

    def swap_popt(self, n1, n2):
        popt = self.gauss_components_popt
        popt[n1 * 3], popt[n2 * 3] = popt[n2 * 3], popt[n1 * 3]
        popt[n1 * 3 + 1], popt[n2 * 3 + 1] = popt[n2 * 3 + 1], popt[n1 * 3 + 1]
        popt[n1 * 3 + 2], popt[n2 * 3 + 2] = popt[n2 * 3 + 2], popt[n1 * 3 + 2]
        self.gauss_components_popt = popt

    def order_popt(self, p_type):
        popt = self.gauss_components_popt
        if p_type == 2:
            if popt[1] < popt[4]:
                pass
            else:
                self.swap_popt(0, 1)
        elif p_type == 3:
            if popt[1] < popt[4] < popt[7]:
                pass
            elif popt[1] < popt[7] < popt[4]:
                self.swap_popt(1, 2)
            elif popt[4] < popt[1] < popt[7]:
                self.swap_popt(0, 1)
            elif popt[4] < popt[7] < popt[1]:
                self.swap_popt(0, 1)
                self.swap_popt(1, 2)
            elif popt[7] < popt[1] < popt[4]:
                self.swap_popt(0, 2)
                self.swap_popt(1, 2)
            elif popt[7] < popt[4] < popt[1]:
                self.swap_popt(0, 2)
            else:
                print('Equal popt')
        self.gauss_components_popt = popt

    def plot_data(self, ax, other=None):
        sigma_scale = 1
        if other is not None:
            assert isinstance(other, Data)
            other.plot_data(ax)
            self.align_profiles_central_core(other)
            self.plot_data(ax)

        if isinstance(self, ObsData) or isinstance(self, SyntheticData):
            colors = ['navy', 'crimson', 'darkgreen']
        else:
            colors = ['royalblue', 'lightcoral', 'limegreen']

        artist = ax.plot(self.x_trans, self.y_intensity, color=colors[0])
        ax.plot(self.x_trans,
                multigauss[str(self.interp_peaks)](self.x_trans, *self.gauss_components_popt),
                color=colors[1], lw=0.7)

        for i, popt_i in enumerate(self.gauss_components_popt):
            modified_popt_up = []
            modified_popt_down = []
            for j, popt_j in enumerate(self.gauss_components_popt):
                if j != i:
                    modified_popt_up.append(popt_j)
                    modified_popt_down.append(popt_j)
                else:
                    modified_popt_up.append(popt_i + sigma_scale * self.p_sigma[i])
                    modified_popt_down.append(popt_i - sigma_scale * self.p_sigma[i])

            ax.fill_between(self.x_trans,
                            multigauss[str(self.interp_peaks)](self.x_trans,
                                                               *modified_popt_down),
                            multigauss[str(self.interp_peaks)](self.x_trans,
                                                               *modified_popt_up),
                            color=colors[1], alpha=0.3)

        for peak in range(self.interp_peaks):
            ax.plot(self.x_trans,
                    gauss(self.x_trans, self.gauss_components_popt[peak * 3],
                          self.gauss_components_popt[peak * 3 + 1],
                          self.gauss_components_popt[peak * 3 + 2]), label='obs fit',
                    color=colors[2], lw=0.7)
            ax.fill_between(self.x_trans,
                            gauss(self.x_trans,
                                  self.gauss_components_popt[peak * 3] - sigma_scale * self.p_sigma[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2]),
                            gauss(self.x_trans,
                                  self.gauss_components_popt[peak * 3] + sigma_scale * self.p_sigma[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2]),
                            color=colors[2], alpha=0.3)
            ax.fill_between(self.x_trans,
                            gauss(self.x_trans, self.gauss_components_popt[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1] - sigma_scale * self.p_sigma[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2]),
                            gauss(self.x_trans, self.gauss_components_popt[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1] + sigma_scale * self.p_sigma[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2]),
                            color=colors[2], alpha=0.3)
            ax.fill_between(self.x_trans,
                            gauss(self.x_trans, self.gauss_components_popt[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2] - sigma_scale * self.p_sigma[peak * 3 + 2]),
                            gauss(self.x_trans, self.gauss_components_popt[peak * 3],
                                  self.gauss_components_popt[peak * 3 + 1],
                                  self.gauss_components_popt[peak * 3 + 2] + sigma_scale * self.p_sigma[peak * 3 + 2]),
                            color=colors[2], alpha=0.3)

        return artist

    def align_profiles_lse(self, other):
        assert isinstance(other, Data)
        y_intensity_interp = interp1d(self.x_trans, self.y_intensity, fill_value='extrapolate')
        loss = 0
        n_shifted_pixels = 0
        n_shifted_pixels_min = 0

        while max(self.x_trans) + n_shifted_pixels * Data.pixel_obs_mas <= other.width_mas:
            loss_temp = 0

            for i in range(len(other.x_trans)):
                loss_temp += (other.y_intensity[i] - y_intensity_interp(
                    other.x_trans[i] - n_shifted_pixels * Data.pixel_obs_mas)) ** 2

            if n_shifted_pixels == 0:
                loss = loss_temp
            elif loss_temp <= loss:
                loss = loss_temp
                n_shifted_pixels_min = n_shifted_pixels

            n_shifted_pixels += 1

        total_shift = n_shifted_pixels_min * Data.pixel_obs_mas
        self.x_trans += total_shift

        for peak in range(self.interp_peaks):
            self.gauss_components_popt[peak * 3 + 1] += total_shift

    def align_profiles_adv(self, other):
        assert isinstance(other, Data)
        shift = other.gauss_components_popt[1] - self.gauss_components_popt[1]
        self.x_trans += shift
        for peak in range(self.interp_peaks):
            self.gauss_components_popt[peak * 3 + 1] += shift

    def align_profiles_central_core(self, other):
        assert isinstance(other, Data)
        shift = other.gauss_components_popt[4] - self.gauss_components_popt[4]
        self.x_trans += shift
        for peak in range(self.interp_peaks):
            self.gauss_components_popt[peak * 3 + 1] += shift


class ObsData(Data):
    def __init__(self, key):
        self.key = key
        extracted_source = Data.obs_data_avail_.iloc[key]['source']
        extracted_pc_in_mas = Data.source_info_[Data.source_info_['source'] == extracted_source]['pc_in_mas'][0]
        Data.pixel_obs_mas = Data.obs_data_avail_.iloc[key]['pixel_obs_mas']
        if Data.source is None:
            Data.source = extracted_source
        else:
            assert Data.source == extracted_source
        if Data.pc_in_mas is None:
            Data.pc_in_mas = extracted_pc_in_mas
        else:
            assert Data.pc_in_mas == extracted_pc_in_mas
        interp_peaks = self.obs_data_avail_.iloc[key]['n_of_peaks']

        y_intensity = np.load(join(self.obs_data_path, self.obs_data_avail_.iloc[key]['path']))['arr_0']
        x_trans = []
        for i in range(len(y_intensity)):
            x_trans.append(i * self.pixel_obs_mas)
        x_trans = np.array(x_trans)
        freq = self.obs_data_avail_.iloc[key]['freq']
        self.bmaj = self.obs_data_avail_.iloc[key]['bmaj']
        self.bmin = self.obs_data_avail_.iloc[key]['bmin']
        self.bpa = self.obs_data_avail_.iloc[key]['bpa']

        super().__init__(x_trans, y_intensity, interp_peaks, freq)


class MHDData(Data):
    # def __init__(self, other, numpy_seed):
    #     self.set_seed(numpy_seed)
    #     MHDData.set_intensity()
    #     MHDData.set_calc_precision()
    #     MHDData.set_g_min()
    #     MHDData.order_magnetic_field()
    #     # Note: the randomness is defined by the np rand seed and, subsequently, the number of samples already prepared.
    #     # Note: one should be extremely careful with the random state in multiprocessing/threading.
    #     self.set_random_parameters(other)
    #
    #     self.x_trans, self.y_intensity = None, None
    #
    #     # MHDDatas, both FittingData and SyntheticData, only need to set a proper randomizer in their `__init__()`.
    #     # So, here we ask the parent initializer to do nothing.
    #     super().__init__(None, None, None, None)

    def set_seed(self, seed):
        self.seed = seed
        random.seed(self.seed)

    @classmethod
    def set_intensity(cls):
        intensity = ''
        if cls.os_() == 'Linux':
            intensity = join(join(join(project_path(), 'crt-engine'), 'bin'), 'intensity ')
            cls._reports = get_path(parent=storage_path(), me='reports')
            cls._partition = 'RT'
        elif cls.os_() == 'Windows':
            intensity = join(join(join(project_path(), 'crt-engine'), 'win'), 'intensity.exe ')
        try:
            assert len(intensity) > 0
        except AssertionError:
            print('The name of the program for intensity is empty.')

        if cls.os_() == 'Linux':
            os.system('chmod 755 ' + intensity)
        cls._intensity = intensity

    @classmethod
    def set_calc_precision(cls, in_step_mult=1, prec_primary=10 ** (-3), prec_add=10 ** (-3), reduce_multiplier=500):
        cls._in_step_mult = in_step_mult
        cls._prec_primary = prec_primary
        cls._prec_add = prec_add
        cls._reduce_multiplier = reduce_multiplier

    @classmethod
    def set_g_min(cls, g_min=1):
        cls._g_min = g_min

    @classmethod
    def order_magnetic_field(cls, ordered=1):
        cls._ordered = ordered

    def set_random_parameters(self, other):
        self.set_random_crosssection()
        self.set_random_heat()
        self.set_mhd_source()
        self.set_random_width(other)
        self.set_beam(other)

        self.set_f_id_hash()
        self.log_(self.f_id)

    def set_random_crosssection(self):
        self.m = 'B'
        self.phi = random.uniform(13.9, 20.5)
        self.sigma_m = [5, 20, 50][random.randint(0, 2)]
        self.p = random.uniform(1.5, 4)  # (1.5, 4)
        # self.d_rl = np.array([25, 50, 100])[random.randint(0, 2)]
        self.d_rl = [25, 50, 100][random.randint(0, 1)]  # [25, 50, 100][random.randint(0, 2)]

    def set_random_heat(self):
        self.k_percent = 10 ** (random.uniform(-4, 0))  #  10 ** (random.uniform(-5, 0))
        self.k_r = random.uniform(0.6, 1) # random.uniform(0.5, 1)

    def set_mhd_source(self):
        self.sp = Data.mhd_data_avail_.loc[(Data.mhd_data_avail_['m'] == self.m)
                                           & (Data.mhd_data_avail_['s'] == self.sigma_m)
                                           & (Data.mhd_data_avail_['d'] == self.d_rl), 'sp'].values[0]
        self.sf = Data.mhd_data_avail_.loc[(Data.mhd_data_avail_['m'] == self.m)
                                           & (Data.mhd_data_avail_['s'] == self.sigma_m)
                                           & (Data.mhd_data_avail_['d'] == self.d_rl), 'sf'].values[0]

        self.mhd_source = join(Data.mhd_data_path, join(self.sp, self.sf))

    def set_random_width(self, other):
        r_jet = np.loadtxt(self.mhd_source, unpack=True, usecols=1)
        d_rl_precise = max(r_jet)
        self.starting_coord = 0
        self.ending_coord = len(r_jet) * 2 + 1
        self.central_coord = len(r_jet) # Not necessarily the thickest part, but one of the thick enough ones.

        width_rl = d_rl_precise * 2

        if other is not None:
            # i.e. self is FittingData instance
            self.shrink_width = random.uniform(0.9, 1.1) # random.uniform(0.5, 2)
            self.width_mas = other.width_mas * self.shrink_width
            self.rl_pc = self.width_mas / width_rl * Data.pc_in_mas
        else:
            self.width_mas = random.uniform(4, 10)
            self.rl_pc = 10 ** (random.uniform(-4, -2))
            self.pc_in_mas = self.rl_pc / self.width_mas * width_rl

        self.rl_mas = self.width_mas / width_rl

    def set_beam(self, other):
        if other is None:
            self.bmaj = random.uniform(0.5, 2)
            self.bmin = random.uniform(0.5, 2)
            self.bpa = random.uniform(0, 180)
        else:
            self.bmaj = other.bmaj
            self.bmin = other.bmin
            self.bpa = other.bpa

    def set_f_id_hash(self):
        self.f_id = '{} {} {} {} {} {} {}'.format(self.phi, self.sigma_m, self.p, self.d_rl, self.rl_pc,
                                                  self.k_percent, self.k_r)

        self.hash = str(hashlib.md5(self.f_id.encode('utf-8')).hexdigest())

    def log_(self, str_):
        lf = open(join(Data.log_save_path, self.hash + '.dat'), 'a+')
        lf.write(str_ + '\n')
        lf.close()

    def gen_out_file(self, freq, psi0, probe_label=''):
        return join(self.out_save_path,
                    'seed_{}__{}_freq_{}__psi0_{}{}.dat'.format(self.seed, self.hash, freq, psi0, probe_label))

    def gen_str_calc(self, freq, psi0, starting_coord=None, ending_coord=None, probe_label=''):
        if starting_coord is None:
            starting_coord = self.starting_coord
        if ending_coord is None:
            ending_coord = self.ending_coord
        str_calc = '{} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}'.format(
            MHDData._intensity,
            self.mhd_source,
            self.gen_out_file(freq, psi0, probe_label),
            self.phi,
            freq,
            psi0,
            self.rl_pc,
            self.k_percent,
            self.k_r,
            sigma_m_B[str(self.sigma_m)],
            self.p,
            MHDData._g_min,
            MHDData._ordered,
            MHDData._prec_primary,
            MHDData._prec_add,
            MHDData._in_step_mult,
            MHDData._reduce_multiplier,
            starting_coord,
            ending_coord
            #self.starting_coord,
            #self.ending_coord
        )
        return str_calc

    def profile_calculate(self, other, psi0):
        if other is not None:
            freq = other.freq
        else:
            freq = SyntheticData.freq
        _out_file = self.gen_out_file(freq, psi0)
        _str_calc = self.gen_str_calc(freq, psi0)
        _out_file_exists = os.path.exists(join(self.out_save_path, _out_file))

        if not _out_file_exists:
            # if self.os_() == 'Linux':
            #     _task = _str_calc
            #     _task_title = self.hash
            #     with open('sub.sh', 'w+') as f:
            #         f.write('#!/bin/bash \n\n#SBATCH -p ' + MHDData._partition
            #                 + ' \n#SBATCH -J ' + _task_title
            #                 + ' \n#SBATCH -N 1 \n#SBATCH -n 1 \n')
            #         f.write('#SBATCH -o ' + join(MHDData._reports, '%j.out') + '\n\n')
            #         f.write(_task)
            #     _ = sub.run('sbatch sub.sh', capture_output=True, text=True, shell=True, check=True)
            #
            #     sleep_time = 60
            #     num_retries = 1440  # minutes in a day
            #     for _ in range(num_retries):
            #         try:
            #             self.x_trans, self.y_intensity = np.loadtxt(_out_file, unpack=True)
            #             str_error = None
            #         except Exception as e:
            #             str_error = str(e)
            #
            #         if str_error:
            #             time.sleep(sleep_time)
            #         else:
            #             if len(self.x_trans) < (
            #                     self.ending_coord - self.starting_coord) / MHDData._reduce_multiplier + 1:
            #                 time.sleep(sleep_time)
            #             else:
            #                 break
            # elif self.os_() == 'Windows':
            #     os.system(_str_calc)

            os.system(_str_calc)

        self.x_trans, self.y_intensity = np.loadtxt(_out_file, unpack=True)
        self.x_trans = -np.flip(self.x_trans) * self.rl_mas
        self.profile_convolve(other)

    def check_DataProcessing_subclasses_to_diff(self, other):
        # We check that the first argument, `self`, is purely MHDData calculated to fit either ObsData or SyntheticData,
        # and `other` is one of them.
        assert isinstance(self, FittingData)
        assert isinstance(other, ObsData) or isinstance(other, SyntheticData)

    def profile_convolve(self, other):
        if other is not None:
            self.check_DataProcessing_subclasses_to_diff(other)

        n_of_pixels_bare = len(self.x_trans)
        if n_of_pixels_bare < Data.CONV_PIXEL_LIMIT:
            n_of_pixels = n_of_pixels_bare
        else:
            # We should use the integer number as a divisor to get proper `n_in_pixel`.
            n_of_pixels = n_of_pixels_bare // 2

        n_in_pixel = n_of_pixels_bare // n_of_pixels
        image_ideal = np.zeros([n_of_pixels, n_of_pixels])
        omega_pixel = (self.width_mas / n_of_pixels) ** 2 * (10 ** (-3) / 3600 * np.pi / 180) ** 2
        omega_pixel *= 10 ** 23

        for i in range(n_of_pixels):
            im_temp = 0
            for j in range(n_in_pixel):
                im_temp += self.y_intensity[n_in_pixel * i + j]
            image_ideal[i] = im_temp / n_in_pixel * omega_pixel

        x = np.arange(0, n_of_pixels, 1)
        y = np.arange(0, n_of_pixels, 1)
        xx, yy = np.meshgrid(x, y, sparse=True)
        x0 = n_of_pixels // 2
        y0 = n_of_pixels // 2

        bmaj = self.bmaj / self.width_mas * n_of_pixels
        bmin = self.bmin / self.width_mas * n_of_pixels
        bpa = self.bpa

        theta = np.deg2rad(-bpa)
        sigma_x = bmin / (2 * np.sqrt(2 * np.log(2)))
        sigma_y = bmaj / (2 * np.sqrt(2 * np.log(2)))
        a = np.cos(theta) ** 2 / (2 * sigma_x ** 2) + np.sin(theta) ** 2 / (2 * sigma_y ** 2)
        b = -np.sin(2 * theta) / (4 * sigma_x ** 2) + np.sin(2 * theta) / (4 * sigma_y ** 2)
        c = np.sin(theta) ** 2 / (2 * sigma_x ** 2) + np.cos(theta) ** 2 / (2 * sigma_y ** 2)
        convolving_beam = np.exp(-(a * (xx - x0) ** 2 + 2 * b * (xx - x0) * (yy - y0) + c * (yy - y0) ** 2))

        # image_conv = fftconvolve(image_ideal, convolving_beam, mode='same')
        image_conv = fftconvolve(image_ideal, convolving_beam, mode='same')
        s_beam = np.pi * bmaj * bmin / (4 * np.log(2))

        # x_trans_conv = np.zeros(2 * n_of_pixels - 1)
        x_trans_conv = np.zeros(n_of_pixels)
        x_pixel = self.width_mas / n_of_pixels

        # for i in range(2 * n_of_pixels - 1):
        for i in range(n_of_pixels):
            x_trans_conv[i] = x_pixel * i

        y_intensity_conv = image_conv[:, n_of_pixels // 2]

        self.y_intensity *= omega_pixel * s_beam

        # plt.plot(self.x_trans, self.y_intensity)
        # plt.plot(x_trans_conv, y_intensity_conv)
        # if other is not None:
        #     plt.plot(other.x_trans, other.y_intensity)
        # plt.show()

        self.x_trans = x_trans_conv
        self.y_intensity = y_intensity_conv

    def initialize_mhd_calculations(self, other, numpy_seed):
        self.set_seed(numpy_seed)
        MHDData.set_intensity()
        MHDData.set_calc_precision()
        MHDData.set_g_min()
        MHDData.order_magnetic_field()
        # Note: the randomness is defined by the np rand seed and, subsequently, the number of samples already prepared.
        # Note: one should be extremely careful with the random state in multiprocessing/threading.
        self.set_random_parameters(other)


class FittingData(MHDData):
    n_samples = 0

    # TODO: ПОСТАВИТЬ НОРМАЛЬНОЕ ЗНАЧЕНИЕ ТОЧНОСТИ
    DIFF_PREC = 0.05  # % / 100
    PSI0_LEFT_INIT = 0  # 10 ** 33 G cm**2 // 0
    PSI0_RIGHT_INIT = 0.1 # 10 ** 33 G cm**2 // 10
    PSI0_INIT = (PSI0_LEFT_INIT + PSI0_RIGHT_INIT) / 2

    PSI0_LEFT_RIGHT_GAP = 0.005  # 10 ** 33 G cm**2 // 0.005
    PSI0_RIGHT_IMPROVE = 1  # 10 ** 33 G cm**2 // 5
    PSI0_TOO_THICK_IMPROVE = 0.5  # % / 100 // 0.5

    def __init__(self, other, seed):
        assert other is not None  # Since it is a fitting data.
        assert isinstance(other, ObsData) or isinstance(other, SyntheticData)
        # super().__init__(other)
        self.initialize_mhd_calculations(other, seed)
        self.interp_peaks = other.interp_peaks
        Data.__init__(self, None, None, None, None)

    @classmethod
    def set_psi0_init(cls, psi0_left_init__10_33_g_cm_2, psi0_right_init__10_33_g_cm_2):
        cls.PSI0_LEFT_INIT = psi0_left_init__10_33_g_cm_2  # 10 ** 33 G cm**2
        cls.PSI0_RIGHT_INIT = psi0_right_init__10_33_g_cm_2  # 10 ** 33 G cm**2
        cls.PSI0_INIT = (cls.PSI0_LEFT_INIT + cls.PSI0_RIGHT_INIT) / 2  # 10 ** 33 G cm**2

    @classmethod
    def set_diff_prec(cls, diff_prec__to_stop_psi0_update__ratio):
        cls.DIFF_PREC = diff_prec__to_stop_psi0_update__ratio  # % / 100

    @classmethod
    def set_right_improve(cls, psi0_left_right_gap__to_improve_psi0_right__10_33_g_cm_2,
                          psi0_right_improve__10_33_g_cm_2):
        cls.PSI0_LEFT_RIGHT_GAP = psi0_left_right_gap__to_improve_psi0_right__10_33_g_cm_2  # 10 ** 33 G cm**2
        cls.PSI0_RIGHT_IMPROVE = psi0_right_improve__10_33_g_cm_2  # 10 ** 33 G cm**2

    @classmethod
    def set_psi0_too_thick_improve(cls, psi0_too_thick_improve__contr_diff_history__ratio):
        cls.PSI0_TOO_THICK_IMPROVE = psi0_too_thick_improve__contr_diff_history__ratio  # % / 100

    def calculate_diff(self, other):
        self.check_DataProcessing_subclasses_to_diff(other)
        diff = 0
        # As the parameters are dimensioned, we use the relative error. `other` is used as a reference as it is
        # observational or synthetic data to fit.
        for i, gcp in enumerate(other.gauss_components_popt):
            diff += (gcp - self.gauss_components_popt[i]) ** 2 / gcp ** 2
        return diff

    def _calculate_diff_central_core(self, other, psi0):
        self.check_DataProcessing_subclasses_to_diff(other)
        '''
        The central core is always presented so the presence of the central peak is the most reliable feature.
        The parameters are height, center coordinate, width; we need height.
        '''
        assert self.interp_peaks == 3

        # self.detect_peaks_gaussian_filter()
        self.detect_peaks_cut_quateredges()
        print('The detected peaks indices are: ', self.detect_peaks_idx)

        self.y_detect_peaks = self.y_intensity[self.detect_peaks_idx]
        self.x_detect_peaks = self.x_trans[self.detect_peaks_idx]
        self.gauss_components()

        if self.os_() == 'Windows':
            plt.plot(self.x_trans, self.y_intensity, color='blue', label='fitter')
            plt.plot(other.x_trans, other.y_intensity, color='green', label='data')
            plt.plot(self.x_trans, gauss(self.x_trans, self.gauss_components_popt[3],
                                         self.gauss_components_popt[4],
                                         self.gauss_components_popt[5]), color='red', label='fitter core')
            plt.plot(other.x_trans, gauss(other.x_trans, other.gauss_components_popt[3],
                                          other.gauss_components_popt[4],
                                          other.gauss_components_popt[5]), color='orange', label='data core')
            plt.title(fr'$\Psi_0 = {psi0}$')
            plt.legend()
            plt.show()

        diff_central_core = ((other.gauss_components_popt[3] - self.gauss_components_popt[3]) /
                             other.gauss_components_popt[3])
        return diff_central_core

    def detect_peaks_gaussian_filter(self):
        sigma_ = 1
        num_retries = len(self.y_intensity)

        for _ in range(num_retries):
            try:
                y_filtered = gaussian_filter1d(self.y_intensity, sigma_)
                self.detect_peaks_idx = find_peaks(y_filtered)[0]
                n_detected = len(self.detect_peaks_idx)
                if self.os_() == 'Windows':
                    plt.plot(self.x_trans, y_filtered, label=str(sigma_))
                    for item in self.x_trans[self.detect_peaks_idx]:
                        plt.axvline(item)
                    plt.legend()
                    plt.title(f'Filtering sigma is {sigma_}, the number of detected peaks is {n_detected}')
                    plt.show()
                assert n_detected <= 3
                break
            except AssertionError:
                sigma_ += 1

    def detect_peaks_cut_quateredges(self):
        w = len(self.y_intensity)
        # plt.plot(self.y_intensity)
        # plt.show()
        self.detect_peaks_idx = find_peaks(self.y_intensity[(w // 4): (3 * w // 4)])[0] + w // 4
        if len(self.detect_peaks_idx) == 0:
            self.detect_peaks_idx = [w // 2]

    def features_file(self, other):
        if self.found_fit:
            features_file = join(storage_path(), "features_train_{}.dat".format(other.hash))
        else:
            features_file = join(storage_path(), "features_not_fit_train_{}.dat".format(other.hash))
        return features_file

    def targets_file(self, other):
        if self.found_fit:
            targets_file = join(storage_path(), "targets_train_{}.dat".format(other.hash))
        else:
            targets_file = join(storage_path(), "targets_not_fit_train_{}.dat".format(other.hash))
        return targets_file

    def probe_central_spectral_index(self, other, psi0):
        freqs = {}
        y_intensity = {}
        
        up = 1.01
        low = 0.99
        if other is not None:
            freqs['u'] = other.freq * up
            freqs['l'] = other.freq * low
        else:
            freqs['u'] = SyntheticData.freq * up
            freqs['l'] = SyntheticData.freq * low
        
        probe_label = 'probe'
        print(freqs)
        for key, freq in freqs.items():
            _out_file = self.gen_out_file(freq, psi0, probe_label)
            _str_calc = self.gen_str_calc(freq, psi0, self.central_coord, self.central_coord + 1, probe_label)
            _out_file_exists = os.path.exists(join(self.out_save_path, _out_file))

            if not _out_file_exists:
                os.system(_str_calc)

            x_trans, y_intensity[key] = np.loadtxt(_out_file, unpack=True)
       
        return np.log(y_intensity['u'] / y_intensity['l']) / np.log(freqs['u'] / freqs['l'])
    
    def test_psi0_init(self, other, psi0_c):
        sp = self.probe_central_spectral_index(other, psi0_c)
        self.log_('psi0 {}, central spectral index {}'.format(psi0_c, sp))
        
        while sp >= 2:
            psi0_c /= 2
            sp = self.probe_central_spectral_index(other, psi0_c)
            self.log_('psi0 {}, central spectral index {}'.format(psi0_c, sp))

        return psi0_c
        
    def fit(self, other):
        # The parameters to use in an iterative total magnetic flux update.
        _psi0 = {
            'l': self.PSI0_LEFT_INIT,
            'r': self.PSI0_RIGHT_INIT,
            'c': self.PSI0_INIT
        }
        
        _psi0['c'] = self.test_psi0_init(other, _psi0['c'])

        _diff = np.inf
        # self.doubled_width = True
        num_retries = 30
        _diff_retry_acceptable = 0.01 # 0.01

        _diff_history = []
        _psi0_history = []

        self.found_fit = False

        while num_retries >= 0:
            _best_cand_psi0 = None
            _psi0_str = ''
            for label in _psi0:
                _psi0_str += str(_psi0[label]) + ' '
            self.log_('psi0 ' + _psi0_str)

            self.profile_calculate(other, _psi0['c'])
            _diff = self._calculate_diff_central_core(other, _psi0['c'])
            self.log_('diff_central_core {}\n'.format(_diff))

            _diff_history.append(_diff)
            _psi0_history.append(_psi0['c'])

            if abs(_diff) <= FittingData.DIFF_PREC:
                _best_cand_psi0 = _psi0['c']
                break
            elif _diff < 0:
                _psi0['r'] = _psi0['c']
                _psi0['c'] = (_psi0['l'] + _psi0['c']) / 2
            elif len(_diff_history) > 1 and 0 < _diff_history[-2] < _diff:
                _psi0 = {
                    'l': self.PSI0_LEFT_INIT,
                    'r': _psi0['c'],
                    'c': self.PSI0_INIT * FittingData.PSI0_TOO_THICK_IMPROVE
                }
            else:
                if _psi0['r'] - _psi0['l'] < FittingData.PSI0_LEFT_RIGHT_GAP:
                    _psi0['r'] += FittingData.PSI0_RIGHT_IMPROVE

                _psi0['l'] = _psi0['c']
                _psi0['c'] = (_psi0['r'] + _psi0['c']) / 2

            # _diff = abs(_diff)

            num_retries -= 1

            if num_retries == 0:
                _diff_history = np.array(_diff_history)
                _best_cand_psi0 = _psi0_history[np.argmin(abs(_diff_history))]
                _best_cand_diff = np.min(abs(_diff_history))
                self.profile_calculate(other, _best_cand_psi0)
                num_retries_diff = 10 ** 3
                for _ in range(num_retries_diff):
                    # try:
                    #     _diff = self._calculate_diff_central_core(other, _best_cand_psi0)
                    #     assert abs(_diff - _best_cand_diff) <= _diff_retry_acceptable
                    #     break
                    # except AssertionError:
                    #     pass
                    _diff = self._calculate_diff_central_core(other, _best_cand_psi0)
                    if abs(_diff - _best_cand_diff) <= _diff_retry_acceptable:
                        break
                break

        # if _best_cand_psi0 is not None:
        #     self.log_('psi0 ' + str(_best_cand_psi0))
        # else:
        #     self.log_('psi0 ' + str(_psi0['c']))

        try:
            assert _best_cand_psi0 is not None
            self.log_('psi0 ' + str(_best_cand_psi0))
        except AssertionError:
            msg = 'Best candidate total magnetic flux is not defined after all possible retries.'
            if self.os_() == 'Windows':
                print(msg)
            self.log_(msg)

        self.log_('diff_central_core {}'.format(_diff))

        # self.doubled_width = False
        FittingData.n_samples += 1

        if abs(_diff) <= FittingData.DIFF_PREC:
            self.found_fit = True

        Data.__init__(self, self.x_trans, self.y_intensity, other.interp_peaks, other.freq)
        fig, ax = plt.subplots(1, 1)
        self.plot_data(ax, other=other)
        if self.os_() == 'Windows':
            plt.show()
        fig.savefig(join(Data.figs_save_path, '{}_{}.jpg'.format(self.hash, other.hash)))

        features_file = self.features_file(other)
        targets_file = self.targets_file(other)

        features = open(features_file, 'a+')
        features.write('{} {} {}\n'.format(self.hash, self.f_id, _best_cand_psi0))
        features.close()
        targets = open(targets_file, 'a+')
        targets.write(str(self.calculate_diff(other)) + '\n')
        targets.close()
        self.log_('===============================\n\n')


class SyntheticData(MHDData):
    freq = 15.4  # Default frequency for synthetic data generation.
    synth_data_path = get_path(storage_path(), 'synth_data')
    synth_data_avail = join(synth_data_path, 'synth_data_available.dat')
    synth_data_columns = ['freq', 'bmaj', 'bmin', 'bpa', 'pixel_obs_mas', 'pc_in_mas', 'n_of_peaks', 'path', 'angle',
                          's', 'd', 'rl_pc', 'k_percent', 'k_r', 'psi0']
    check = True
    if check:
        synth_data_avail_ = pd.read_csv(synth_data_avail, header=None, sep=' ', usecols=range(len(synth_data_columns)))
        synth_data_avail_.columns = synth_data_columns

    def __init__(self, key, seed=None):
        if key is None:
            other = None
            self.initialize_mhd_calculations(other, seed)
            self.psi0 = 10 ** random.uniform(-1, 1)  # (-1, 2)
            self.profile_calculate(other, self.psi0)

            # As far, the data construction implies three peaks.
            super().__init__(x_trans=self.x_trans,
                             y_intensity=self.y_intensity,
                             # interp_peaks=None,
                             interp_peaks=3,
                             freq=SyntheticData.freq)

            self.save_synthetic()
        else:
            self.key = key
            extracted_pc_in_mas = SyntheticData.synth_data_avail_.iloc[key]['pc_in_mas']
            SyntheticData.pixel_obs_mas = SyntheticData.synth_data_avail_.iloc[key]['pixel_obs_mas']
            if Data.pc_in_mas is None:
                Data.pc_in_mas = extracted_pc_in_mas
            else:
                assert Data.pc_in_mas == extracted_pc_in_mas
            interp_peaks = self.synth_data_avail_.iloc[key]['n_of_peaks']

            x_trans, y_intensity = np.loadtxt(join(self.synth_data_path, self.synth_data_avail_.iloc[key]['path']),
                                              unpack=True)
            freq = self.obs_data_avail_.iloc[key]['freq']
            self.bmaj = self.obs_data_avail_.iloc[key]['bmaj']
            self.bmin = self.obs_data_avail_.iloc[key]['bmin']
            self.bpa = self.obs_data_avail_.iloc[key]['bpa']

            self.hash = SyntheticData.synth_data_avail_.iloc[key]['path'][:-4]

            super().__init__(x_trans, y_intensity, interp_peaks, freq)
            Data.gauss_components(self)
            Data.show_gauss_components(self)

    def save_synthetic(self):
        data = np.column_stack([self.x_trans, self.y_intensity])
        path_ = join(SyntheticData.synth_data_path, self.hash + '.dat')
        np.savetxt(path_, data)
        pixel_obs_mas = self.width_mas / len(self.x_trans)
        params = "{} {} {} {} {} {} {} {} {} {}\n".format(SyntheticData.freq, self.bmaj, self.bmin, self.bpa,
                                                          pixel_obs_mas, self.pc_in_mas, self.interp_peaks,
                                                          self.hash + '.dat', self.f_id, self.psi0)
        synth_data_avail_ = open(SyntheticData.synth_data_avail, 'a+')
        synth_data_avail_.write(params)
        synth_data_avail_.close()

def run():
    SyntheticData_instance = SyntheticData(key=0)
    FittingData_instance = FittingData(SyntheticData_instance, seed=random.randint(0, 10**4))
    FittingData_instance.fit(SyntheticData_instance)
    