def verify_requested_columns(columns):
    print('Verify (\'yes\') that the requested fields are:')
    for i in range(len(columns)):
        print(columns[i])
    assert input() == 'yes'


def write_line(line, file):
    out = open(file, 'a+')
    out.write(line)
    out.write('\n')
    out.close()


# def add_data():
#     print('Please verify that added data is placed in the directory ' + obs_data_path)
#     print('Fill in:')
#     line = ''
#
#     print('The source (M87 etc.):')
#     line += input() + ' '
#
#     print('The observational frequency in GHz:')
#     line += input() + ' '
#
#     print('The separation from the core in mas:')
#     line += input() + ' '
#
#     print('The beam in mas, bmaj, bmin and bpa, space-separated:')
#     line += input() + ' '
#
#     print('The observational pixel in mas:')
#     line += input() + ' '
#
#     # TODO: plot the profile to verify the choice.
#     print('Interpreted number of peaks:')
#     line += input() + ' '
#
#     print('The path (ensure single quotes):')
#     path_temp = input()
#     print('Subfolder/subfile?')
#     input_temp = input()
#     while input_temp != 'no':
#         path_temp = join(path_temp, input_temp)
#         print('Subfolder/subfile?')
#         input_temp = input()
#     line += path_temp
#
#     verify_requested_columns(obs_data_columns)
#     write_line(line, obs_data_avail_file)
#
#     print('Add more?')
#     if input() == 'yes':
#         add_data()
#
#
# def add_source():
#     print('Fill in:')
#     line = ''
#
#     print('The source (M87 etc.):')
#     line += input() + ' '
#
#     print('Luminosity distance (pc in mas):')
#     line += input()
#
#     verify_requested_columns(source_columns)
#     write_line(line, source_info_file)
#
#     print('Add more?')
#     if input() == 'yes':
#         add_source()
